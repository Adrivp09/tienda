<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use  Illuminate\Support\Facades\DB;
class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'nombre' => 'Camiseta',
            'precio' => 5,
            'stock' => 5,
            'descripcion' => 'description product',
            'image' => base64_encode(file_get_contents('/home/itb/tienda/img/cami.png')),
        ]);

        DB::table('products')->insert([
            'nombre' => 'Camiseta_1',
            'precio' => 16,
            'stock' => 5,
            'descripcion' => 'description product',
            'image' => base64_encode(file_get_contents('/home/itb/tienda/img/cami.png')),
        ]);

        DB::table('products')->insert([
            'nombre' => 'Camiset_2',
            'precio' => 24.99,
            'stock' => 5,
            'descripcion' => 'description product',
            'image' => base64_encode(file_get_contents('/home/itb/tienda/img/cami.png')),
        ]);
    }
}
